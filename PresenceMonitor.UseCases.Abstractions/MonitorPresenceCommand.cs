using MediatR;

public record MonitorPresenceCommand() : IRequest;