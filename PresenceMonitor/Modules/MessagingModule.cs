using Configuration;
using Dapr.Client;
using Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using PresenceMonitor.Utilities.Ioc;

namespace Modules;

public static class MessagingModule
{
    public static void Configure(
        IServiceCollection serviceCollection,
        IHostEnvironment hostEnvironment,
        IConfiguration configuration
    ) => serviceCollection
        .AddDaprClient()
        .AddTransient(typeof(IMessagePublisher<,>), typeof(DaprMessagePublisher<,>))
        .AddMonitorPresenceWorker();

    private static IServiceCollection AddDaprClient(this IServiceCollection serviceCollection) => serviceCollection
        .AddOption<DaprClientOptions>()
        .AddTransient<IDaprClientProvider, DaprClientProvider>()
        .AddSingleton<DaprClient>(serviceProvider =>
        {
            var options = serviceProvider.GetRequiredService<IOptions<DaprClientOptions>>();
            return new DaprClientBuilder()
                .UseGrpcEndpoint(options.Value.DaprEndpoint)
                .Build();
        });

    private static IServiceCollection AddMonitorPresenceWorker(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddOption<MonitorPresenceWorkerOptions>();
        serviceCollection.AddHostedService<MonitorPresenceWorker>();
        return serviceCollection;
    }
}