namespace Configuration;

public sealed class DaprClientOptions
{
    public string? DaprEndpoint { get; set; }
}